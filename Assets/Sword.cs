﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Sword : Product
{

    Mesh sword;
    Vector3[] vertices;

    private void Awake()
    {
        foreach (Transform child in transform)
        {
            if (child.name == "Blade")
            {
                sword = child.GetComponent<MeshFilter>().mesh;
                vertices = sword.vertices;
            }
        }
    }

    public override void Hit(float xPosition, float yPostion)
    {
        //get a list of all z values
        float[] zValues = new float[vertices.Length];
        for (int zIndex = 0; zIndex < vertices.Length; zIndex++)
        {
            zValues.SetValue(vertices[zIndex].y, zIndex);
        }

 
        //get the position the hammer is nearest to hitting
        float hitValue = Mathf.Lerp(zValues.Min(), zValues.Max(), xPosition);
        float closest = zValues.OrderBy(item => Mathf.Abs(hitValue - item)).First();

        List<Vector3> hitPoints = new List<Vector3>();
        for (int pointIndex = 0; pointIndex < vertices.Length; pointIndex++)
        {
            if (vertices[pointIndex].y == closest)
            {
                if (closest == zValues.Max())
                {
                    vertices[pointIndex] = new Vector3(0, closest, 0);
                }
                else
                {
                    vertices[pointIndex] = new Vector3(vertices[pointIndex].x, closest, 0);
                }
            }
        }

        sword.vertices = vertices;
        sword.RecalculateBounds();

    }
}
