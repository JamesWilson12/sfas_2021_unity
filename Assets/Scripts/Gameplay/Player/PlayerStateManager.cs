﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStateManager : MonoBehaviour
{

    public enum PlayerState { FreeRoam, Interacting, Activity }
    private PlayerState _state;
    public PlayerState State { get => _state;}

    //Player Interfaces - all of the tools that a player has that are managed by its state
    PlayerMovement movementInterface;
    PlayerLook lookInterface;


    private void Awake()
    {
        movementInterface = GetComponent<PlayerMovement>();
        lookInterface = GetComponentInChildren<PlayerLook>();
    }


    public void setPlayerState(PlayerState state)
    {
        switch (state)
        {
            case PlayerState.FreeRoam:
                movementInterface.setActive(true);
                lookInterface.setActive(true);
                break;
            case PlayerState.Interacting:
                movementInterface.setActive(false);
                lookInterface.setActive(true);
                break;
            case PlayerState.Activity:
                Debug.Log("ACTIVITY STATE");
                movementInterface.setActive(false);
                lookInterface.setActive(false);
                break;
            default:
                throw new System.NotImplementedException(state.ToString() + " is not implemented for setPlayerState in PlayerStateManager");
        }



    }
}
