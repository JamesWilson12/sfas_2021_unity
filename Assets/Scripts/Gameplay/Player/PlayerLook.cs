﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{
    private Transform _playerBody;
    private Camera _playerCamera;

    private float _lookSpeed;
    private float _verticalRotation;

    private Transform _lookingAt;
    private bool _active;
    



    private void Awake()
    {
        _playerBody = transform.parent;
        _playerCamera = GetComponent<Camera>();
        _active = true;

        _lookSpeed = 400f;
        _verticalRotation = 0f;
        

        Cursor.lockState = CursorLockMode.Locked;
        
    }




    // Update is called once per frame
    void Update()
    {
        if (_active)
        {
            ResolveMouseLook();
            QueryInteractable();
        }
        else
        {
            //if disabled, query release
            QueryActivityRelease();
        }

    }

    private void QueryActivityRelease()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if (_lookingAt != null)
            {
                InteractableObject interactableInterface = _lookingAt.GetComponent<InteractableObject>();
                PlayerStateManager stateManager = GetComponentInParent<PlayerStateManager>();

                if (interactableInterface.IsTransitioned)
                {
                    interactableInterface.resetTransition();
                    stateManager.setPlayerState(PlayerStateManager.PlayerState.FreeRoam);
                }

            }
        }
    }

    private void QueryInteractable()
    {
        PlayerStateManager stateManager = GetComponentInParent<PlayerStateManager>();

        if (Input.GetKey(KeyCode.E))
        {
            if (_lookingAt != null)
            {
                InteractableObject interactableInterface = _lookingAt.GetComponent<InteractableObject>();

                if (interactableInterface.IsTransitioned)
                {
                    //Move player to activity state - disables freeroam abilities
                    stateManager.setPlayerState(PlayerStateManager.PlayerState.Activity);
                }
                else
                {
                    //Currently doing a transition
                    if (stateManager.State == PlayerStateManager.PlayerState.FreeRoam)
                    {
                        //first time, set state to interacting
                        stateManager.setPlayerState(PlayerStateManager.PlayerState.Interacting);
                    }

                    interactableInterface.transition();
                }

            }
        }
        else
        {
            if (_lookingAt != null)
            {
                _lookingAt.GetComponent<InteractableObject>().resetTransition();
                stateManager.setPlayerState(PlayerStateManager.PlayerState.FreeRoam);
            }
            QueryPlayerFocus();
        }



    }

    private void QueryPlayerFocus()
    {
        //Finds what the player is looking at and whether it is interactable

        RaycastHit hit;
        Ray ray = _playerCamera.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hit))
        {
            Transform objectHit = hit.transform;
            if (objectHit != _lookingAt)
            {

                //no longer looking at previous object
                if (_lookingAt != null)
                {
                    _lookingAt.GetComponent<InteractableObject>().lookedAtEffect(false);
                    _lookingAt = null;
                }

                InteractableObject lookedAtInterface = objectHit.GetComponent<InteractableObject>();
                if (lookedAtInterface != null)
                {
                    //Player is looking at somethign interactable
                    _lookingAt = objectHit;
                    lookedAtInterface.lookedAtEffect(true);
                }



            }
        }
        else
        {
            if (_lookingAt != null)
            {
                _lookingAt.GetComponent<InteractableObject>().lookedAtEffect(false);
                _lookingAt = null;
            }
        }
    }

    private void ResolveMouseLook()
    {
        //Collect Mouse change between frames
        float mouseX = Input.GetAxis("Mouse X") * _lookSpeed * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * _lookSpeed * Time.deltaTime;

        //track current vertical rotation to avoid looking backwards
        _verticalRotation -= mouseY;
        _verticalRotation = Mathf.Clamp(_verticalRotation, -90f, 90f);

        //perform transform
        transform.localRotation = Quaternion.Euler(_verticalRotation, 0, 0);
        _playerBody.Rotate(Vector3.up * mouseX);
    }



    public void setActive(bool active)
    {
        _active = active;
    }


}
