﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerMovement : MonoBehaviour
{

    private CharacterController _playerController;
    private float _playerMovementSpeed;
    private float _sprintMultiplier;
    private float _gravity;
    private float _jumpHeight;
    private float _airMovementMultiplier;

    //How close to the ground until player stops falling
    private float _fallingThreshold;

    private Vector3 _velocity;

    [SerializeField]
    private LayerMask groundMask;
    private bool _isGrounded;
    private Vector3 _floorCheck;
    private bool _active;


    private void Awake()
    {
        _playerController = GetComponent<CharacterController>();

        _playerMovementSpeed = 12f;
        _sprintMultiplier = 1.5f;
        _gravity = -20f;
        _jumpHeight = 0.0001f;
        _airMovementMultiplier = 0.8f;
        _fallingThreshold = 0.1f;
        _active = true;
    }






// Update is called once per frame
void Update()
    {
        if (_active)
        {
            ResolveMovement();
            ResolveGravity();
        }
        
    }

    private void ResolveGravity()
    {
        _floorCheck = transform.position - (Vector3.up * (_playerController.bounds.size.y / 2.25f));
        _isGrounded = Physics.CheckSphere(_floorCheck, _fallingThreshold,groundMask);

        if (_isGrounded && _velocity.y < 0)
        {
            _velocity.y = -0.05f;
        }


        if (Input.GetButtonDown("Jump") && _isGrounded)
        {
            _velocity.y = Mathf.Sqrt(_jumpHeight * -2f * _gravity);
        }


        _velocity.y += _gravity * (Time.deltaTime * Time.deltaTime);
        _playerController.Move(_velocity);
    
    
    
    
    }

    private void ResolveMovement()
    {
        //Read Input Values
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");


        //Compute Transform
        Vector3 movementVector =
            transform.right * x
            + transform.forward * z;


        //player sprinting
        if (Input.GetKey(KeyCode.LeftShift))
        {
            movementVector *= _sprintMultiplier;
        }


        //reduces movement in the air
        if (!_isGrounded)
        {
            movementVector *= _airMovementMultiplier;
        }

        _playerController.Move(movementVector * _playerMovementSpeed * Time.deltaTime);
    }


    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(_floorCheck, _fallingThreshold);
    }



    public void setActive(bool active)
    {
        _active = active;
    }

}
