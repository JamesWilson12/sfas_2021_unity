﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WoodStation : ToolActivity
{
    private void Awake()
    {
        _interactableInterface = GetComponent<InteractableObject>();
        _toolObject = GetComponentInChildren<Tool>();
        _productPosition = transform.position + (Vector3.up * 3);

        _currentProduct = null;
        _stationActive = false;
    }


    private void Update()
    {
        if (_stationActive)
        {
            if (_interactableInterface.IsTransitioned)
            {
                ResolveInput();
            }
            else
            {
                ResetStation();
            }
        }
        else
        {
            if (_interactableInterface.IsTransitioned)
            {
                ActivateStation();
            }
            else
            {
                ResetStation();
            }
        }

    }

    public void ResetStation()
    {
        if (_currentProduct != null)
        {
            Destroy(_currentProduct.transform.gameObject);
        }
        CleanUpObjects();
        _toolObject.Reset();
        _stationActive = false;
    }

    public void ActivateStation()
    {

        _toolObject.enabled = true;
        _stationActive = true;
        SpawnProduct();
    }


    protected override void ResolveMouseDown(bool mouseDown)
    {
        if (mouseDown)
        {
            if (_currentProduct != null)
            {
                if (_toolObject._state == Tool.ToolState.Dormant)
                {
                    _toolObject._state = Tool.ToolState.Charging;
                    CleanUpObjects();
                }
                else if (_toolObject._state == Tool.ToolState.Charging)
                {
                    _toolObject.ChargeTool(true);
                }
            }
        }
        else
        {
            if (_toolObject._state == Tool.ToolState.Charging)
            {
                _toolObject.ChargeTool(false);
            }
            else if (_toolObject._state == Tool.ToolState.Charged)
            {
                //Hit
                _toolObject.Reset();
                _currentProduct.Hit(_toolObject.getRelativeToolPosition().z,0);

                Destroy(_currentProduct.transform.gameObject);
                Invoke("SpawnProduct", 2);
            }

        }
    }


    protected override void ResolveMouseMovement(float deltaX, float deltaY)
    {
        _toolObject.SetToolPosition(deltaX, deltaY);
    }

    private void CleanUpObjects()
    {
        foreach (Transform child in transform)
        {
            if (child.name == "LogSplit")
            {
                Destroy(child.gameObject);
            }
        }

    }
}

