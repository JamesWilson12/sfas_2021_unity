﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Tool : MonoBehaviour
{
    protected Vector3 _toolPosition;
    public enum ToolState { Charging, Hitting, Dormant, Charged }
    public ToolState _state;




    public abstract void SetToolPosition(float deltaX, float deltaY);
    public abstract void ChargeTool(bool charge);

    public abstract void Reset();
    public abstract void Hit();
    public abstract Vector3 getRelativeToolPosition();
}
