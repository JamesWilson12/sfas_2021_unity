﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Woodlog : Product
{
    private const float minSideScale = 0.4f;
    private const float maxSideScale = 1f;

    [SerializeField]
    private GameObject splitLogPrefab;


    // Start is called before the first frame update
    private void Awake()
    {
        SetRandomDimensions();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SetRandomDimensions()
    {
        System.Random rand = new System.Random(Time.frameCount);

        float xSide = (float)(minSideScale + (rand.NextDouble() * (maxSideScale - minSideScale)));
        float zSide = (float)(minSideScale + (rand.NextDouble() * (maxSideScale - minSideScale)));

        //logs that are small, are naturally tall
        float deltaY = Mathf.Lerp(minSideScale, maxSideScale, 0.5f) - Mathf.Lerp(xSide, zSide, 0.5f);


        this.transform.localScale = new Vector3(xSide, 1.5f + deltaY, zSide);
    }

    public override void Hit(float xPosition, float yPostion)
    {
        Debug.Log(xPosition);
        xPosition = Mathf.Lerp(0.1f, 0.9f, xPosition);

        GameObject leftLog = Instantiate(splitLogPrefab, transform.position, Quaternion.identity, transform.parent);
        GameObject rightLog = Instantiate(splitLogPrefab, transform.position, Quaternion.identity, transform.parent);

        leftLog.name = "LogSplit";
        rightLog.name = "LogSplit";
        

        leftLog.transform.localScale = new Vector3(transform.localScale.x,
            transform.localScale.y,
            transform.localScale.z*(xPosition));

        leftLog.transform.position = new Vector3(transform.position.x,
            transform.position.y,
            transform.position.z - (transform.localScale.z * (xPosition)/2));

        rightLog.transform.localScale = new Vector3(transform.localScale.x,
            transform.localScale.y,
            transform.localScale.z*(1-xPosition));

        rightLog.transform.position = new Vector3(transform.position.x,
            transform.position.y,
            transform.position.z + (transform.localScale.z * (1-xPosition) / 2));

        leftLog.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, -500));
        rightLog.GetComponent<Rigidbody>().AddForce(new Vector3(0, 0, 500f));

        
        Debug.Log("Split Spawned");

        transform.gameObject.SetActive(false);
    }
}
