﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ToolActivity : MonoBehaviour
{
    //A toolActivity requires a tool prefab to use and a product prefab to work on
    protected float __maxPositionShift;
    protected float _toolMovementSpeed;
    protected float _toolWobble;


    protected Tool _toolObject;
    [SerializeField]
    protected GameObject _productPrefab;
    protected InteractableObject _interactableInterface;
    protected Vector3 _productPosition;
    protected Product _currentProduct;

    protected bool _stationActive;

    protected abstract void ResolveMouseDown(bool mouseDown);
    protected abstract void ResolveMouseMovement(float deltaX, float deltaY);


    protected void ResolveInput()
    {
        ResolveMouseMovement(Input.GetAxis("Mouse X") * Time.deltaTime, Input.GetAxis("Mouse Y") * Time.deltaTime);
        ResolveMouseDown(Input.GetMouseButton(0));
    }



    protected void SpawnProduct()
    {
        if (_currentProduct == null)
        {
            if (_stationActive)
            {
                _currentProduct = Instantiate(_productPrefab, _productPosition, _productPrefab.transform.rotation, transform).GetComponent<Product>();
            }
        }
    }
}
