﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Axe : Tool
{
    private  float _maxPositionShift = 1;
    private  float _maxRotationShift = 500;
    private float _toolMovementSpeed = 5;
    private float _toolWobble = 0.1f;

    private float _chargeValue = 0;
    private float _chargeRate = 0.005f;
    


    private float _positionShift = 0;
    private float _rotationShift = 0;


    private void Awake()
    {
        enabled = false;
        _state = ToolState.Dormant;
        _toolPosition = transform.position;
    }

    public override void SetToolPosition(float deltaX, float deltaY)
    {
        if (deltaX != 0)
        {
            _positionShift = Mathf.Clamp(_positionShift - (deltaX * _toolMovementSpeed), -_maxPositionShift, _maxPositionShift);
        }

        //Axe Postion
        transform.position = _toolPosition + new Vector3(
            Mathf.Cos(Time.time*2) * _toolWobble/2f,
            (Mathf.Sin(Time.time*2)*_toolWobble) +  _chargeValue * 2,
            _positionShift);
        //Axe Rotation
        transform.rotation = Quaternion.Euler(transform.rotation.eulerAngles.x,
            _positionShift*-10,
            -90f + _chargeValue*30);
    }

    

    public override void ChargeTool(bool charge)
    {
        if (charge)
        {
            _chargeValue = Mathf.Clamp(_chargeValue + _chargeRate, 0, 1);
        }
        else
        {
            _chargeValue = Mathf.Clamp(_chargeValue - _chargeRate*2, 0, 1);
        }

        if (_chargeValue == 1)
        {
            _state = ToolState.Charged;
        }
        else if (_chargeValue == 0)
        {
            _state = ToolState.Dormant;
        }
    }

    public override void Reset()
    {
        _state = ToolState.Dormant;
        _chargeValue = 0;
        enabled = false;
        
    }

    public override void Hit()
    {
        //NEEDS IMPLEMENTING
        _state = ToolState.Dormant;
        _chargeValue = 0;
    }

    public override Vector3 getRelativeToolPosition()
    {
        //return vector of tool shift values scaled between 0 and 1
        return new Vector3(0, 0, (_positionShift + _maxPositionShift) / (_maxPositionShift*2));

    }
}
