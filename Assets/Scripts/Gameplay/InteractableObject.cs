﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    public Camera objectCamera;
    public GameObject highlightEffect;
    private Camera _playerCamera;

    private Vector3 _cameraStartLocation;
    private Quaternion _cameraStartDirection;

    private Vector3 _cameraTargetLocation;
    private Quaternion _cameraTargetDirection;

    private float _cameraTransitionSpeed;
    private float _transitionProgress;
    public bool IsTransitioned { get { return _transitionProgress == 1; } }


    private void Awake()
    {
        objectCamera.enabled = false;
        
        lookedAtEffect(false);
        _playerCamera = Camera.main;

        _transitionProgress = 0f;
        _cameraTransitionSpeed = 0.005f;
        _cameraTargetLocation = objectCamera.transform.position;
        _cameraTargetDirection = objectCamera.transform.rotation;
        
    
    
    }



    public void lookedAtEffect(bool lookedAt)
    {
        highlightEffect.SetActive(lookedAt);
    }


    public void transition()
    {
        if (_transitionProgress == 0)
        {
            Debug.Log("Start Transtion");
            //new transtion
            _playerCamera.enabled = false;
            objectCamera.enabled = true;

            _cameraStartLocation = _playerCamera.transform.position;
            _cameraStartDirection = _playerCamera.transform.rotation;

            objectCamera.transform.position = _cameraStartLocation;
            objectCamera.transform.rotation = _cameraStartDirection;


            _transitionProgress += _cameraTransitionSpeed * Time.deltaTime;

        }
        else if (_transitionProgress > 0 && _transitionProgress < 1)
        {
            //progress transition
            objectCamera.transform.position = Vector3.Lerp(_cameraStartLocation, _cameraTargetLocation, _transitionProgress);
            objectCamera.transform.rotation = Quaternion.Lerp(_cameraStartDirection, _cameraTargetDirection, _transitionProgress);

            _transitionProgress = Mathf.Clamp(_transitionProgress + _cameraTransitionSpeed, 0, 1);

        }
        else
        {
            //complete transtion
            Debug.Log("Transition Complete");
        }

    }

    public void resetTransition()
    {
        if (_transitionProgress != 0)
        {
            _playerCamera.enabled = true;
            objectCamera.enabled = false;

            objectCamera.transform.position = _cameraTargetLocation;
            objectCamera.transform.rotation = _cameraTargetDirection;

            _transitionProgress = 0;
            Debug.Log("Transition Reset");

        }

    }











}
